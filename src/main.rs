mod lib;

fn main() {
    if let Err(e) = run() {
        eprintln!("Error: {}", e);
        std::process::exit(1);
    }
}

fn run() -> Result<(), Box<dyn std::error::Error>> {
    // Generate dummy data
    let data = lib::generate_dummy_data();
    
    // Process data
    for record in &data {
        // Perform some processing on the record
        println!("{:?}", record);
    }

    Ok(())
}
