use rand::Rng;

#[derive(Debug)]
pub struct Record {
    id: u32,
    name: String,
    value: f64,
}

pub fn generate_dummy_data() -> Vec<Record> {
    let mut rng = rand::thread_rng();
    let mut data = Vec::new();

    for i in 0..10 {
        let record = Record {
            id: i + 1,
            name: format!("Name {}", i + 1),
            value: rng.gen_range(0.0..100.0),
        };
        data.push(record);
    }

    data
}

pub fn process_data(record: &Record) -> String {
    // Perform some processing on the record and return the result
    format!("ID: {}, Name: {}, Value: {}", record.id, record.name, record.value)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_process_data() {
        let record = Record {
            id: 1,
            name: String::from("Ayush"),
            value: 42.0,
        };
        let result = process_data(&record);
        assert_eq!(result, "ID: 1, Name: Ayush, Value: 42");
    }
}
