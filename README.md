# Rust Command-Line Tool with Dummy Data

This project is a simple Rust command-line tool that demonstrates data processing functionality using internally generated dummy data. It includes data ingestion, processing, and unit tests.

## Functionality

The program performs the following tasks:

1. **Dummy Data Generation**: It generates dummy data internally instead of reading from a CSV file.
2. **Data Processing**: It processes each record of the dummy data, performing some custom processing logic on it.
3. **Output**: The processed data is printed to the console.

## Usage

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/ag758/week8_mini_project.git
    ```

2. Navigate to the project directory:

    ```bash
    cd week8_mini_project
    ```

3. Build the project using Cargo:

    ```bash
    cargo build
    ```

4. Run the executable:

    ```bash
    cargo run
    ```
5. Test 
    ```bash
    cargo test
    ```
## Features

- **Dummy Data Generation**: Generates dummy data internally for demonstration purposes.
- **Data Processing**: Applies custom processing logic to each record of the generated dummy data.
- **Unit Tests**: Includes unit tests to ensure the correctness of the processing logic.

## Functionality Details

### Dummy Data Generation

The dummy data generation logic is implemented in the `generate_dummy_data` function in `lib.rs`. It generates a vector of dummy records, each containing an ID, a name, and a value.

### Unit Tests

Unit tests are included to verify the correctness of the processing logic. The `lib.rs` file contains a test module with a unit test for the `process_data` function. This test ensures that the processing logic behaves as expected for a given input record.

## Dependencies

- [rand](https://crates.io/crates/rand): Used for generating random data for testing purposes.

## Structure

The project structure is as follows:


- `src/`: Contains the Rust source code.
- `src/main.rs`: Entry point for the command-line tool.
- `src/lib.rs`: Additional functionality and unit tests.
- `Cargo.toml`: Project manifest file specifying dependencies and metadata.

### Output

The processed data is printed to the console. Each record is printed in a formatted manner.

![run](Screenshots/run.png)

### Unit Tests

Unit tests are included to verify the correctness of the processing logic. The `lib.rs` file contains a test module with a unit test for the `process_data` function. This test ensures that the processing logic behaves as expected for a given input record.

![test](Screenshots/test.png)

## Contributing

Contributions are welcome! Feel free to open issues or submit pull requests.

